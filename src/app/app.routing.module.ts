import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ErrorComponent } from './components/error/error.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { VideosListComponent } from './components/videos-list/videos-list.component';
import { VideoNewComponent } from './components/video-new/video-new.component';
import { VideoDetailComponent } from './components/video-detail/video-detail.component';
import { VideoEditComponent } from './components/video-edit/video-edit.component';
import {VideoSearchComponent} from './components/video-search/video-search.component';
import {UsersListComponent} from './components/users-list/users-list.component';
import {UserVideosListComponent} from './components/user-videos-list/user-videos-list.component';

//Guard de identidad
import {identityGuard} from './services/identity.guard';


const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user-edit', component: UserEditComponent,canActivate:[identityGuard] },
    { path: 'users-list',component:UsersListComponent},
    { path: 'myVideos', component: VideosListComponent,canActivate:[identityGuard] },
    { path: 'myVideos/:page', component: VideosListComponent,canActivate:[identityGuard] },
    { path: 'video/add', component: VideoNewComponent,canActivate:[identityGuard] },
    { path: 'video/detail/:videoId', component: VideoDetailComponent },
    { path: 'video/edit/:videoId', component: VideoEditComponent,canActivate:[identityGuard] },
    { path: 'video/search/:searchString',component:VideoSearchComponent},
    { path: 'videos-list/:userId/:page',component:UserVideosListComponent},
    { path: '**', component: ErrorComponent },
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);