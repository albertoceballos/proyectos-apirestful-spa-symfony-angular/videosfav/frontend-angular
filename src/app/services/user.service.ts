import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
//url api
import { GLOBAL } from './global';
//modelo
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url;
  public identity;
  public token;

  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL.url;

  }

  register(user): Observable<any> {
    let json = JSON.stringify(user);
    let params = "json=" + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this._httpClient.post(this.url + 'register', params, { headers: headers });

  }

  login(user, getToken = null): Observable<any> {
    if (getToken != null) {
      user.getToken = 'true';
    }
    let json = JSON.stringify(user);
    let params = "json=" + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this._httpClient.post(this.url + 'login', params, { headers: headers });
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));

    if (identity != null && identity != "" && identity != "undefined") {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;
  }

  getToken() {
    let token = localStorage.getItem('token');
    if (token != null && token != "" && token != "undefined") {
      this.token = token;
    } else {
      this.token = null
    }

    return this.token;
  }

  userEdit(user,token):Observable<any>{
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);

    return this._httpClient.put(this.url+'user/edit',params,{headers:headers});
  }

  getUsers():Observable<any>{
    
    return this._httpClient.get(this.url+'users');
  }

}
