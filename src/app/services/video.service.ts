import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';

//servicio de usuario
import {UserService} from './user.service';
//url global API
import {GLOBAL} from '../services/global';


@Injectable({
  providedIn: 'root'
})
export class VideoService {
  public url:string;
  public identity;
  public token;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   //videos del usuario logueado
   myVideos(token,page):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);
    return this._httpClient.get(this.url+'video/list?page='+page,{headers:headers});
   }

   //videos de cualquier usuario
   userVideos(userId,page):Observable<any>{
     return this._httpClient.get(this.url+'videoUser/list?user='+userId+'&page='+page);
   }

   addNewVideo(token,video):Observable<any>{
     let json=JSON.stringify(video);
     let params="json="+json;
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);

     return this._httpClient.post(this.url+'video/new',params,{headers:headers});
   }

   detailVideo(token,videoId):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpClient.get(this.url+'video/detail/'+videoId,{headers:headers});

   }

   deleteVideo(token,videoId):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpClient.delete(this.url+'video/remove/'+videoId,{headers:headers});
   }

   updateVideo(token,videoId,video):Observable<any>{
     let json=JSON.stringify(video);
     let params="json="+json;
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);

     return this._httpClient.put(this.url+'video/update/'+videoId,params,{headers:headers});

   }

   search(search=""):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let params="searchString="+search;
    return this._httpClient.post(this.url+'video/search',params,{headers:headers});

   }



  
}
