import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

//servicios
import { UserService } from '../../services/user.service';
import { VideoService } from '../../services/video.service';

//modelos
import { Video } from '../../models/Video';
import { from } from 'rxjs';

@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  styleUrls: ['./videos-list.component.css'],
  providers: [UserService, VideoService],

})
export class VideosListComponent implements OnInit {

  public title: string;
  public identity;
  public token;
  public status;
  public videos: Array<Video>;
  public page: number;
  public pages: number;
  public arrayPages;
  public total_items: number;
  public deleteStatus;
  public videoIds;

  constructor(private _userService: UserService, private _videoService: VideoService,private _activatedRoute:ActivatedRoute) {
    this.title = "Mi lista de videos";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {

    this._activatedRoute.params.subscribe(
      params=>{
        var page=params['page'];
        this.myVideos(this.token,page);
      }
    );


   
  }

  myVideos(token, page = 1) {
    this._videoService.myVideos(token, page).subscribe(
      response => {
        if (response.status == 'success') {
          this.videos = response.videos;
          
          //en un array meto las ids de youtube de cada video con un índice que es el id del video en la BBDD
          this.videoIds=[];
          for(let i=0;i<this.videos.length;i++){
            let idYoutube=this.videos[i].url.split("/").pop();
            this.videoIds[this.videos[i].id]=idYoutube;
          }
            console.log(this.videoIds);
              
          this.page = response.page;
          this.pages = response.pages;
          this.arrayPages = [];
          for (let i = 1; i <= this.pages; i++) {
            this.arrayPages.push(i);
          }
          console.log(response);
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );
  }

  deleteVideo(videoId){
    this._videoService.deleteVideo(this.token,videoId).subscribe(
      response=>{
        if(response.status=='success'){
          this.deleteStatus='success';
          console.log(response);
          this.myVideos(this.token);
        }else{
          this.deleteStatus='error';
        }
      },
      error=>{
        console.log(error);
        this.deleteStatus='error';
      }
    );
  }

}
