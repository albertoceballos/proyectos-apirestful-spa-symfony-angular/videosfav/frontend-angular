import { Component, OnInit } from '@angular/core';

import {ActivatedRoute,Router,Params} from '@angular/router';

//servicios
import {UserService} from '../../services/user.service';
import {VideoService} from '../../services/video.service';

//modelos
import {Video} from '../../models/Video';

@Component({
  selector: 'app-video-edit',
  templateUrl: './video-edit.component.html',
  styleUrls: ['./video-edit.component.css'],
  providers: [UserService,VideoService],
})
export class VideoEditComponent implements OnInit {
  public pageTitle;
  public token;
  public video:Video;
  public videoId;
  public status;
  constructor(private _userService:UserService, 
    private _videoService:VideoService,
    private _activatedRoute:ActivatedRoute) {
    this.pageTitle="Editar vídeo";
    this.token=this._userService.getToken();
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.videoId=params['videoId'];
        this._videoService.detailVideo(this.token,this.videoId).subscribe(
          response=>{
            if(response.video){
              this.video=response.video;
             
            }
            console.log(response);
          },
          error=>{
            console.log(error);
          }
        );
      }
    );
  }

  onSubmit(){
    this._videoService.updateVideo(this.token,this.videoId,this.video).subscribe(
      response=>{
        if(response.status=='success'){
          this.status='success';
          console.log(response.video);
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
      }
    );
  }


}
