import { Component, OnInit } from '@angular/core';

//modelos
import {Video} from '../../models/Video';

//servicios
import {UserService} from '../../services/user.service';
import {VideoService} from '../../services/video.service';

@Component({
  selector: 'app-video-new',
  templateUrl: './video-new.component.html',
  styleUrls: ['./video-new.component.css'],
  providers:[VideoService],
})
export class VideoNewComponent implements OnInit {
  public pageTitle:string;
  public identity;
  public token;
  public video:Video;
  public status:string;
  public message:string;
  constructor(private _videoService:VideoService,private _userService:UserService) {
    this.pageTitle="Añadir vídeo";
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.video=new Video('',this.identity.sub,'','','','normal','','');
    this.video.url="http://";
   }

  ngOnInit() {
  
  }

  onSubmit(){
    console.log(this.video);
    this._videoService.addNewVideo(this.token, this.video).subscribe(
      response=>{
        if(response.status){
          console.log(response.video);
          this.status='success';
          setTimeout(()=>{
            this.status=null;
          },3000);

        }else{
          this.status='error';
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
  }

}
