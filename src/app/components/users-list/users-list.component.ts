import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';

//modelos
import {User} from '../../models/User';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
  providers:[UserService],
})
export class UsersListComponent implements OnInit {
  public pageTitle;
  public users;
  public status;
  public numVideos;
  constructor(private _userService:UserService) {
    this.pageTitle="Usuarios";
   }
  
  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this._userService.getUsers().subscribe(
      response=>{
        if(response.status=='success'){
          this.users=response.users;
          console.log(response.users);
          this.status='success';
          this.numVideos=[];

        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
  }

}
