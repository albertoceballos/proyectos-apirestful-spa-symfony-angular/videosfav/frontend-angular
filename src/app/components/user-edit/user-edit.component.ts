import { Component, OnInit } from '@angular/core';

//servicio
import {UserService} from '../../services/user.service';
//modelo
import {User} from '../../models/User';

import {GLOBAL} from '../../services/global';

import * as $ from 'jquery';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public title:string;
  public identity;
  public token;
  public user;
  public status;
  public message;
  public afuConfig;
  public url;
  constructor(private _userService:UserService) {
    this.title="Editar Usuario";
    this.url=GLOBAL.url;
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.user=new User(this.identity.name,this.identity.surname,this.identity.email,'','','','');

    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png,.jpeg,.gif",
      maxSize: "20",
      uploadAPI: {
        url: this.url + "user/image",
        headers: { "Authorization": this.token }
      },
      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: 'Select Files',
        resetBtn: 'Reset',
        uploadBtn: 'Upload',
        dragNDropBox: 'Drag N Drop',
        attachPinBtn: 'Sube tu foto...',
        afterUploadMsg_success: 'Foto subida con éxito!',
        afterUploadMsg_error: 'Error al subir la foto, inténtalo de nuevo !'
      }
    };


   }

  ngOnInit() {
  
  }

  onSubmit(){
    this._userService.userEdit(this.user,this.token).subscribe(
      response=>{
        if(response.status=='success'){
          this.status='success';
          this.identity=response.user;
          console.log(response.user);
          localStorage.setItem('identity',JSON.stringify(this.identity));
          setTimeout(()=>{
            $(".alert-success").fadeOut("slow",()=>{this.status=null});
          },2500);
        }else{
          this.status='error';
          setTimeout(()=>{
            $(".alert-danger").fadeOut("slow",()=>{this.status=null});
          },2500);
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
  }

  uploadImage(data){

    let data_obj=JSON.parse(data.response);
    this.user.avatar=data_obj.user.avatar;
    console.log(this.user);
  }
}
