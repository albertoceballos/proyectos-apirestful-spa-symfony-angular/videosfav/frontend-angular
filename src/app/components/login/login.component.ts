import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
//servicios
import { UserService } from '../../services/user.service';
//modelos
import { User } from '../../models/User';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService],
})
export class LoginComponent implements OnInit {

  public title: string;
  public status: string;
  public token: string;
  public identity;
  public user: User;
  constructor(private _userService: UserService, private _router: Router) {
    this.title = "Login";
    this.user = new User('', '', '', '', '', '','');
  }

  ngOnInit() {
  }

  onSubmit() {

    this._userService.login(this.user).subscribe(
      response => {
        this.identity = response.data;
        console.log(response.data);
        if (response.status = 'success') {
          this._userService.login(this.user, true).subscribe(
            response => {
              this.token = response.data;
              if (response.status == 'success') {
                localStorage.setItem('identity', JSON.stringify(this.identity));
                localStorage.setItem('token', this.token);
                this.status="success";
                setTimeout(() => {
                  this._router.navigate(['/home']);
                }, 2500);
              }else{
                this.status="error";
              }

            },
            error => {
              console.log(error);
              this.status="error";
            }
          );
        }
      },
      error => {
        console.log(error);
      }
    );

  }

}
