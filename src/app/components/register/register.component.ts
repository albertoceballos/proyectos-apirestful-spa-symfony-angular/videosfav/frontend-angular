import { Component, OnInit } from '@angular/core';

//servicios
import {UserService} from '../../services/user.service';
//modelos
import {User} from '../../models/User';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService],
})
export class RegisterComponent implements OnInit {

  public title;
  public user;
  public status;
  public message;
  constructor(private _userService:UserService) {
    this.title="Regístrate";
    this.user=new User('','','','','ROLE_USER','','');

   }

  ngOnInit() {
   
  }

  onSubmit(form){
    this._userService.register(this.user).subscribe(
      response=>{
        if(response.user){
          this.status="success";
          form.reset();
        }else{
          this.status="error";
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status="error";
      }
    );
  }

}
