import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { Video } from '../../models/Video';
import { VideoService } from '../../services/video.service';

@Component({
  selector: 'app-user-videos-list',
  templateUrl: './user-videos-list.component.html',
  styleUrls: ['./user-videos-list.component.css'],
  providers: [VideoService],
})
export class UserVideosListComponent implements OnInit {
  public title;
  public status;
  public page;
  public pages;
  public total;
  public videos: Array<Video>
  public arrayPages;
  public videoIds;
  public userId;
  constructor(private _videoService: VideoService,private _activatedRoute:ActivatedRoute) {
    this.title = "Videos del usuario";
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.userId=params['userId'];
        let page=params['page'];
        this.userListVideos(this.userId,page);
      }
    );
  }

  userListVideos(userId, page = 1) {
    this._videoService.userVideos(userId, page).subscribe(
      response => {
        if (response.status = "success") {
          this.videos = response.videos;

          //en un array meto las ids de youtube de cada video con un índice que es el id del video en la BBDD
          this.videoIds = [];
          for (let i = 0; i < this.videos.length; i++) {
            let idYoutube = this.videos[i].url.split("/").pop();
            this.videoIds[this.videos[i].id] = idYoutube;
          }
          console.log(this.videoIds);

          this.page = response.page;
          this.pages = response.pages;
          this.arrayPages = [];
          for (let i = 1; i <= this.pages; i++) {
            this.arrayPages.push(i);
          }
          console.log(response);
        } else {
          this.status = 'error';
        }

      },
      error => {
        console.log(error);
      }
    );
  }

}
