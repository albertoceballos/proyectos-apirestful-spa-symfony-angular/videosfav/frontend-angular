import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserVideosListComponent } from './user-videos-list.component';

describe('UserVideosListComponent', () => {
  let component: UserVideosListComponent;
  let fixture: ComponentFixture<UserVideosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserVideosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserVideosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
