import { Component, OnInit, } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

//modelo
import { Video } from '../../models/Video';

//servicios
import { UserService } from '../../services/user.service';
import { VideoService } from '../../services/video.service';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css'],
  providers: [UserService, VideoService],
})
export class VideoDetailComponent implements OnInit {
  public pageTitle: string;
  public identity;
  public token;
  public video:Video;
  public status;
  public videoUrl;
  public trustedUrl;
  constructor(private _userService: UserService, 
    private _videoService: VideoService,
    private _activatedRoute:ActivatedRoute,
    private _router:Router,
    private _sanitazer:DomSanitizer) {
    this.pageTitle = "Detalle de vídeo";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        let videoId=params['videoId'];
        this._videoService.detailVideo(this.token,videoId).subscribe(
          response=>{
            if(response.video){
              this.video=response.video;
              this.videoUrl=this.video.url;
              console.log(response);
              console.log(this.videoUrl);
              //saco el id del video de Youtube:
              let id=this.videoUrl.split("/").pop();
              this.trustedUrl= this._sanitazer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+id);
            }else{
              this.status="error";
            }
          },
          error=>{
            console.log(error);
            this.status='error';
          }
        )

      }
    );
  }

  deleteVideo(videoId){
    this._videoService.deleteVideo(this.token,videoId).subscribe(
      response=>{
        if(response.status=='success'){
          window.alert("Vídeo borrado con éxito");
          this._router.navigate(['/myVideos']);
        }else{
          window.alert("No se pudo borrar el vídeo");
        }
      },
      error=>{
        console.log(error);
      }
    );

  }


}
