import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { Video } from '../../models/Video';
import { VideoService } from '../../services/video.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-video-search',
  templateUrl: '../videos-list/videos-list.component.html',
  styleUrls: ['./video-search.component.css'],
  providers: [VideoService, UserService],
})
export class VideoSearchComponent implements OnInit {
  public title: string;
  public videos: Array<Video>;
  public videoIds:Array<any>;
  public message;
  public searchStatus;
  public identity;
  constructor(private _videoService: VideoService, private _activatedRoute: ActivatedRoute, private _userService:UserService) {
    this.title = "Resultado de la búsqueda";
    this.identity=this._userService.getIdentity();
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        let searchString = params['searchString'];
        console.log(searchString);
        this._videoService.search(searchString).subscribe(
          response => {
            if (response.status == 'success') {
              console.log(response);
              this.videos = response.videos;
              this.message=response.results+" Resultados encontrados";
              this.searchStatus='success';

              //en un array meto las ids de youtube de cada video con un índice que es el id del video en la BBDD
              this.videoIds = [];
              for (let i = 0; i < this.videos.length; i++) {
                let idYoutube = this.videos[i].url.split("/").pop();
                this.videoIds[this.videos[i].id] = idYoutube;
              }
            } else {
              this.message = response.message;
              this.searchStatus='error';
              this.videos=null;
            }
          },
          error => {
            console.log(error);
          }
        );
      }
    );
  }

}
