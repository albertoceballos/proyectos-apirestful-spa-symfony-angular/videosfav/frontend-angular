import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//modulo para formularios
import {FormsModule} from '@angular/forms';

import{HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ErrorComponent } from './components/error/error.component';

//file-uploader
import {AngularFileUploaderModule} from 'angular-file-uploader';
//angular2-moment
import {MomentModule} from 'angular2-moment';
//routing
import {appRoutingProviders,routing} from './app.routing.module';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { VideosListComponent } from './components/videos-list/videos-list.component';
import { VideoNewComponent } from './components/video-new/video-new.component';
import { VideoDetailComponent } from './components/video-detail/video-detail.component';
import { VideoEditComponent } from './components/video-edit/video-edit.component';
import { VideoSearchComponent } from './components/video-search/video-search.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UserVideosListComponent } from './components/user-videos-list/user-videos-list.component';

//guard de indentidad
import {identityGuard} from './services/identity.guard';
import {UserService} from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    ErrorComponent,
    UserEditComponent,
    VideosListComponent,
    VideoNewComponent,
    VideoDetailComponent,
    VideoEditComponent,
    VideoSearchComponent,
    UsersListComponent,
    UserVideosListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule,
    MomentModule,
    routing,
  ],
  providers: [appRoutingProviders,identityGuard,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
