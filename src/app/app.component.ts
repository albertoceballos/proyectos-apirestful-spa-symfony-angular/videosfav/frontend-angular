import { Component,DoCheck } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';

//servicios
import {UserService} from './services/user.service';
import {VideoService} from './services/video.service';
//ruta
import {GLOBAL} from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService,VideoService],
})
export class AppComponent implements DoCheck {
  public title;
  public identity;
  public token;
  public url;
  public search;


  constructor(private _userService:UserService,private _videoService:VideoService, private _router:Router){
    this.title="VideosFav";
    this.url=GLOBAL.url;
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    console.log(this.url);
  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
  }

  logout(){
    localStorage.clear();
    this.identity=null;
    this.token=null;
    this._router.navigate(['/login']);
  }

  onSubmit(){
    console.log(this.search);
    this._router.navigate(['/video/search',this.search]);
    
  }
}
