export class Video{
    public id:number;
    public userId:number;
    public title:string;
    public description:string;
    public url:string;
    public status:string;
    public createdAt:string;
    public updatedAt:string;

    constructor(id,userId,title,description,url,status,createdAt,updatedAt){
        this.id=id;
        this.userId=userId;
        this.title=title;
        this.description=description;
        this.url=url;
        this.status=status;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }

}