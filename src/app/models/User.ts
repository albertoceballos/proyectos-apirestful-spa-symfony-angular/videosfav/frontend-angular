
export class User{
    public id:number;
    public name:string;
    public surname:string;
    public email:string;
    public password:string;
    public role:string;
    public createdAt:string;
    public avatar:string;

    constructor(name,surname,email,password,role,createdAt,avatar){
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.password=password;
        this.role=role;
        this.createdAt=createdAt;
        this.avatar=avatar;
    }

}